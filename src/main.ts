import 'phaser';
import Breakout from './breakout';

/*
  Configuration de PhaserJS
 https://photonstorm.github.io/phaser3-docs/Phaser.Types.Core.html#.GameConfig
 */
const config: Phaser.Types.Core.GameConfig = {
  type: Phaser.AUTO, // AUTO, WEBGL ou CANVAS
  width: 800, height: 600, // Taille du canvas
  parent: 'phaser-canvas', // élement HTML qui contiendra le canvas 
  scene: [ Breakout ], // JS Classe contenant les fonctions: preload, create, update
  physics: { // Configuration de la physique
    default: 'arcade' // arcade ou 
  }
};

var game = new Phaser.Game(config);
