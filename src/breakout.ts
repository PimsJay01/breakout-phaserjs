import Ball from './scripts/ball';
import Bricks from './scripts/bricks';
import Paddle from './scripts/paddle';

export default class Breakout extends Phaser.Scene {
  bricks: Bricks;
  paddle: Paddle;
  ball: Ball;
  
  constructor(config: Phaser.Types.Core.GameConfig) {
    super(config);
  }

  // Function de PhaserJS permettant de charger toutes les ressources nécessaires
  preload() {
    this.load.atlas('assets', 'assets/breakout.png', 'assets/breakout.json');
  }

  // Function de PhaserJS pour définir l'état initial de la scène
  create() {
    this.bricks = new Bricks(this);
    this.paddle = new Paddle(this);
    this.ball = new Ball(this);

    // Collisions
    // https://photonstorm.github.io/phaser3-docs/Phaser.Physics.Arcade.Factory.html#collider__anchor
    this.physics.add.collider(this.ball.ballImage, this.bricks.group, this.bricks.hitBrick.bind(this.bricks), null, this);
    this.physics.add.collider(this.ball.ballImage, this.paddle.image, this.ball.hitPaddle.bind(this.paddle), null, this);
    //  Active les collisions sur la bordure sauf au sol
    this.physics.world.setBoundsCollision(true, true, true, false);

    // Evénements
    this.bricks.on("allBricksDestroyedEvent", this.resetLevel.bind(this));
    this.paddle.on("paddleMovedEvent", this.ball.paddleMoved.bind(this.ball));
    this.ball.on("outOfBoundsEvent", this.outOfBounds.bind(this));
  }

  // Function de PhaserJS pour la mise à jour de la scène (Boucle de gameplay)
  update() {
    this.ball.update();
  }

  resetLevel() {
    this.ball.resetPosition(this.paddle.image);
    this.bricks.reset();
  }

  outOfBounds() {
    this.ball.resetPosition(this.paddle.image);
  }
};
