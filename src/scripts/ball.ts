const VELOCITY_FACTOR = 10;

// https://photonstorm.github.io/phaser3-docs/Phaser.GameObjects.GameObject.html
export default class Ball extends Phaser.GameObjects.GameObject {

  ballImage: Phaser.Physics.Arcade.Image;
  onPaddle: Boolean;
  
  constructor(scene: Phaser.Scene) {
    super(scene, "ball");

    this.create();
  }

  create() {
    const canvas = this.scene.game.canvas;
    this.ballImage = this.scene.physics.add.image(canvas.width / 2, canvas.height - 100, 'assets', 'ball1')
      .setCollideWorldBounds(true)
      .setBounce(1);
    this.onPaddle = true;

    this.scene.input.on('pointerup', () => {

      if (this.onPaddle)
      {
        this.ballImage.setVelocity(-75, -300);
        this.onPaddle = false;
      }

    }, this);
  }

  resetPosition(paddleImage: Phaser.Physics.Arcade.Image) {
    this.ballImage.setVelocity(0);
    this.ballImage.setPosition(paddleImage.x, this.scene.game.canvas.height - 100);
    this.onPaddle = true;
  }

  hitPaddle(ballImage: Phaser.Physics.Arcade.Image, paddleImage: Phaser.Physics.Arcade.Image) {
    var diff = 0;

    if (ballImage.x < paddleImage.x)
    {
      //  Ball is on the left-hand side of the paddle
      diff = paddleImage.x - ballImage.x;
      ballImage.setVelocityX(-VELOCITY_FACTOR * diff);
    }
    else if (ballImage.x > paddleImage.x)
    {
      //  Ball is on the right-hand side of the paddle
      diff = ballImage.x -paddleImage.x;
      ballImage.setVelocityX(VELOCITY_FACTOR * diff);
    }
    else
    {
      //  Ball is perfectly in the middle
      //  Add a little random X to stop it bouncing straight up!
      ballImage.setVelocityX(2 + Math.random() * 8);
    }
  }

  paddleMoved(paddleImage: Phaser.Physics.Arcade.Image) {
    if (this.onPaddle)
    {
        this.ballImage.x = paddleImage.x;
    }
  }

  update() {
    if (this.ballImage.y > this.scene.game.canvas.height)
    {
        this.emit("outOfBoundsEvent");
    }
  }

};
